import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import VueSSE from 'vue-sse';
import VueToast from 'vue-toast-notification';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueSSE)
Vue.use(VueToast);

new Vue({
  render: h => h(App),
}).$mount('#app')

