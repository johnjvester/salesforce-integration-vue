import axios from 'axios'

const SERVER_URL = 'http://localhost:9999';

const instance = axios.create({
    baseURL: SERVER_URL,
    timeout: 1000
});

export default {
    getContacts: () => instance.get('/contacts', {
        transformResponse: [function (data) {
            return data ? JSON.parse(data) : data;
        }]
    })
}