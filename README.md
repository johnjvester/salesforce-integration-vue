# salesforce-integration-vue Repository

> The `salesforce-integration-vue` repository is very basic [Vue.js](https://vuejs.org/) client application
> designed to make RESTful calls to the [`salesforce-integration-service`](https://gitlab.com/johnjvester/salesforce-integration-service)
> repository, which interacts with a [Salesforce](http://www.salesforce.com) instance in order to retrieve (GET) 
> `Contact` objects and also listen to server-sent events (SSEs) - updating the contact list dynamically and also 
> displaying an information toast message regarding the update that was received via the SSE payload.

## Publications

This repository is related to an article published on DZone.com:

* TBD

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939

## Important Information

This is a very quick Svelte application, which expects the `salesforce-integration-service` to be running at the following
URL:

```shell
http://localhost:9999/contacts
```

For now, if this value needs to change, simply update the following sections of code in the `Contacts.vue` file:

```javascript
sseClient = this.$sse.create({
    url: 'http://localhost:9999/stream/' + uuidv4(),
    format: 'json',
    withCredentials: false,
    polyfill: true,
});
```
And also the `contact-service.js` file:

```javascript
const SERVER_URL = 'http://localhost:9999';
```

## Project Dependencies

The following dependencies are part of this project:

* [`bootstrap-vue`](https://bootstrap-vue.org) - Vue.js adaptation of the most popular CSS Framework for developing responsive websites.
* [`vue-sse`](https://www.npmjs.com/package/vue-sse) - enables effortless use of server-sent events for Vue.js applications.
* [`axios`](https://www.npmjs.com/package/axios) - promise based HTTP client for the browser and node.js.

## Using This Repository

To use this repository, simply follow the instructions in the [`salesforce-integration-service`](https://gitlab.com/johnjvester/salesforce-integration-service)  repository, then issue the following command:

```bash
npm install
npm run serve
```

Navigate to [localhost:8080/](http://localhost:8080/) and a screen similar to what is listed below will appear:

![Contact List Default](./ContactListBefore.png)

At this point, the current list of contacts will be displayed.  

Now, if a `PATCH` command is performed to the Sean Forbes contact (as an example) to change the title from "CFO" to "CEO":

```shell
curl --location --request PATCH 'http://localhost:9999/contacts/0035e000008eXq1AAE' \
--header 'Content-Type: application/json' \
--data-raw '{
    "Title": "CEO"
}'
```

The new title will automatically appear in the contact list, and an informational toast message will be displayed:

![Contact List After SSE Arrives](./ContactListAfter.png)

Both of these events will be processed as a result of the arrival of a SSE message containing the updated contact.

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
